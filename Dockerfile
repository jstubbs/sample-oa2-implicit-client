FROM node:0.10
MAINTAINER Joe Stubbs <jstubbs@tacc.utexas.edu>

# install bower
RUN npm install --global bower grunt-cli 

# add application configuration
ADD package.json /package.json
ADD bower.json /bower.json
ADD Gruntfile.coffee /Gruntfile.coffee

# install JSO
RUN bower install jso --save --allow-root

# add the application itself
ADD agavejs.html /agavejs.html

# install grunt locally
RUN npm install

# run the server by default
CMD ["grunt", "server", "--force"]
